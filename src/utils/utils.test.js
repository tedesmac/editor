import { genId, getBlock, setBlock } from "./utils";

// Test Data
const page = {
  id: "page",
  path: [],
  data: [
    {
      id: "c1",
      path: ["page"],
      data: [
        {
          id: "i1",
          path: ["page", "c1"],
          data: "axioma.jpg",
          settings: [],
          style: []
        },
        {
          id: "c2",
          path: ["page", "c1"],
          data: [
            {
              id: "l1",
              path: ["page", "c1", "c2"],
              data: {
                href: "http://www.meetpuzzle.com",
                text: "puzzle"
              },
              settings: [],
              style: []
            },
            {
              id: "l2",
              path: ["page", "c1", "c2"],
              data: {
                href: "http://www.google.com",
                text: "google"
              },
              settings: [],
              style: []
            },
            {
              id: "l3",
              path: ["page", "c1", "c2"],
              data: {
                href: "http://www.reddit.com",
                text: "reddit"
              },
              settings: [],
              style: []
            }
          ],
          settings: [
            {
              name: "direction",
              data: "horizontal",
              type: "OPTION"
            }
          ],
          style: []
        }
      ],
      settings: [
        {
          name: "direction",
          data: "vertical",
          type: "OPTION"
        }
      ],
      style: [],
      type: "CONTAINER"
    }
  ],
  settings: [],
  style: [],
  type: "PAGE"
};

// genId

test("Tests genId", () => {
  const regex = /^\w{4}-\w{4}-\w{4}-\w{4}$/;
  const id = genId();
  const match = id.match(regex);
  expect(match).not.toBeNull();
});

// getBlock

test("Gets nested link block", () => {
  const block = getBlock(page, ["page", "c1", "c2", "l1"]);
  expect(block.data.text).toEqual("puzzle");
});

test("Gets first level block", () => {
  const block = getBlock(page, ["page", "c1"]);
  expect(block.id).toEqual("c1");
});

test("Gets empty block", () => {
  const block = getBlock(page, ["page", "c1", "c2", "c3"]);
  expect(block).toEqual({});
});

// setBlock

test("Change first child", () => {
  const block = {
    id: "c3",
    path: ["page"],
    data: [],
    settings: [],
    style: []
  };
  const newPage = setBlock(page, ["page", "c1"], block);
  expect(newPage).toEqual({
    ...page,
    data: [block]
  });
});
