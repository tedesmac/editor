import BlockTypes from "./blockTypes";
import Settings from "./settings";

const block = (
  data = null,
  type = "",
  settings = [],
  style = [
    Settings.color("background", "#03A9F4"),
    Settings.color("color", "#000000"),
    Settings.number("height", 100, [
      {
        name: "Percent",
        value: "%"
      },
      {
        name: "em",
        value: "em"
      },
      {
        name: "Pixels",
        value: "px"
      },
      {
        name: "Viewport",
        value: "vh"
      }
    ]),
    Settings.number("width", 100, [
      {
        name: "Percent",
        value: "%"
      },
      {
        name: "em",
        value: "em"
      },
      {
        name: "Pixels",
        value: "px"
      },
      {
        name: "Viewport",
        value: "vw"
      }
    ])
  ]
) => ({
  id: Math.floor(Math.random() * 2 ** 32),
  data: data,
  type: type,
  settings: settings,
  style: style
});

const container = () =>
  block([], BlockTypes.CONTAINER, [
    Settings.option("direction", "vertical", ["horizontal", "vertical"])
  ]);

const image = () => block("", BlockTypes.IMAGE);

const link = () => block({ href: "", text: "" }, BlockTypes.LINK);

const markdown = () => block("", BlockTypes.MARKDOWN);

const string = () => block("", BlockTypes.STRING);

export default { block, container, image, link, markdown, string };
