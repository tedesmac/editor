import SettingTypes from "./settingTypes";

export default {
  color: (name = "", data = "#FFFFFF") => ({
    name: name,
    data: data,
    type: SettingTypes.COLOR
  }),

  number: (name = "", data = 0, units = []) => ({
    name: name,
    data: data,
    units: units,
    type: SettingTypes.NUMBER
  }),

  option: (name = "", data = "", options = []) => ({
    name: name,
    data: data,
    type: SettingTypes.OPTION,
    options: options
  }),

  string: (name = "", data = "") => ({
    name: name,
    data: data,
    type: SettingTypes.STRING
  }),

  url: (name = "", data = "") => ({
    name: name,
    data: data,
    type: SettingTypes.URL
  })
};
