import Vue from "vue";
import Vuex from "vuex";
import App from "./components/app";
import { getBlock, setBlock, setChildren } from "./utils/utils";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    blocks: [],
    blockSettings: [],
    blockStyle: [],
    description: "",
    id: 0,
    mode: "edit",
    page: {
      id: "page",
      path: [],
      data: [],
      settings: [
        {
          name: "direction",
          data: "vertical"
        }
      ],
      style: [],
      type: "PAGE"
    },
    screen: "desktop",
    settingsCallback: null,
    styleCallback: null,
    tab: 0,
    title: "",
    unselectCallback: null
  },

  mutations: {
    setBlock(state, payload) {
      const newPage = setBlock(state.page, payload.path, payload.block);
      state.page = newPage;
    },

    selectBlock(state, payload) {
      if (state.unselectCallback != null) {
        state.unselectCallback();
      }
      payload.selectCallback();
      state.blockSettings = payload.settings;
      state.blockStyle = payload.style;
      state.settingsCallback = payload.settingsCallback;
      state.styleCallback = payload.styleCallback;
      state.unselectCallback = payload.unselectCallback;
      if (payload.tab != null) {
        state.tab = payload.tab;
      }
    },

    setSettings(state, settings) {
      if (state.settingsCallback != null) {
        state.settingsCallback(settings);
      }
    },

    setStyle(state, style) {
      if (state.styleCallback != null) {
        state.styleCallback(style);
      }
    }
  }
});

new Vue({
  el: "#app",
  components: { App },
  store,
  template: "<App />"
});
