const getBlock = (object, path) => {
  if (object.id === path[0] && path.length === 1) {
    return object;
  }
  path = path.slice(1);
  const parent_id = path[0];
  for (let i = 0; i < object.data.length; i++) {
    const child = object.data[i];
    if (parent_id === child.id) {
      return getBlock(child, path);
    }
  }
  return {};
};

const genId = () => {
  let res = "";
  for (let i = 1; i <= 16; i++) {
    const c = Math.floor(Math.random() * 36);
    if (c < 10) {
      res = res.concat(String.fromCharCode(c + 48));
    } else {
      res = res.concat(String.fromCharCode(c + 55));
    }
    if (i % 4 === 0 && i !== 16) {
      res = res.concat("-");
    }
  }
  return res;
};

const setBlock = (object, path, new_object) => {
  if (path[0] === object.id && path.length === 1) {
    return new_object;
  }
  const new_path = path.slice(1);
  for (let i = 0; i < object.data.length; i++) {
    const child = object.data[i];
    if (child.id === new_path[i]) {
      object.data[i] = setBlock(child, new_path, new_object);
      break;
    }
  }
  return object;
};

export { getBlock, genId, setBlock };
